from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from .models import Provider, Offer, OffersProduct, Product
from .forms import ProviderForm


def index(request):
    if request.user.is_authenticated:
        return redirect("sections")
    else:
        return redirect("login")


def providers_list(request):
    all_providers = Provider.objects.all()
    context = {'all_providers': all_providers}
    return render(request, "providers.html", context)


def offers_list(request):

    all_offers = Offer.objects.all()
    all_offersproduct = OffersProduct.objects.all()
    all_products = Product.objects.all()
    providers = Provider.objects.all()

    if 'provider' in request.GET and request.GET['provider']:
        search = request.GET['provider']
        all_offers = Offer.objects.filter(provider__id=search)

    context = {'all_offers': all_offers, 'all_offersproduct': all_offersproduct, 'all_products': all_products, 'all_providers': providers}
    return render(request, "offers.html", context)





def add_provider(request):
    form = ProviderForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            phone = form.cleaned_data['phone']
            new_provider = Provider.objects.create(
                            name=name, email=email, phone=phone, created_by=request.user)
            new_provider.save()
            return redirect("providers_list")

    return render(request, "add_provider.html", {'form': form})


def signup(request):
    form = UserCreationForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('login')
    return render(request, "singup.html", {'form': form})
