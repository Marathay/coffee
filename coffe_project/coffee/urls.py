from django.urls import path
from django.views.generic.base import TemplateView


from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('signup/', views.signup, name='signup'),
    path('sections/', TemplateView.as_view(template_name='sections.html'), name='sections'),
    path('sections/providers/', views.providers_list, name='providers_list'),
    path('sections/offers/', views.offers_list, name='offers'),
    path('sections/providers/add_provider/', views.add_provider, name='add_provider')
]