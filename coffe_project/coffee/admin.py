from django.contrib import admin
from .models import Provider, Product, Offer, OffersProduct
# Register your models here.

admin.site.register(Provider)
admin.site.register(Product)
admin.site.register(Offer)
admin.site.register(OffersProduct)
