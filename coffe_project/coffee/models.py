from django.db import models
from django.contrib.auth.models import User
from datetime import datetime


class Provider(models.Model):
    name = models.CharField(max_length=250)
    email = models.EmailField(max_length=50)
    phone = models.CharField(max_length=20)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    create_date = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    product_name = models.CharField(max_length=250)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    weight = models.IntegerField
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    create_date = models.DateTimeField(default=datetime.now, blank=True)
    description = models.CharField(max_length=250)

    def __str__(self):
        return self.product_name


class Offer (models.Model):
    name = models.CharField(max_length=250)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    create_date = models.DateTimeField(default=datetime.now, blank=True)
    description = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class OffersProduct(models.Model):
    offer_id = models.ForeignKey(Offer, on_delete=models.CASCADE)
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField
    price = models.IntegerField
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    create_date = models.DateTimeField(default=datetime.now, blank=True)
