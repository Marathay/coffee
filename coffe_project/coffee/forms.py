from django import forms


class ProviderForm(forms.Form):
    name = forms.CharField(max_length=250)
    email = forms.EmailField(max_length=50)
    phone = forms.CharField(max_length=20)
